#!/usr/bin/env perl

use strict;
use warnings;
use v5.10;
use Calendar::Simple;

my $year   = shift
	or die qq('年 月 入力ファイル' の形で使用してください);
my $month  = shift
	or die qq('年 月 入力ファイル' の形で使用してください);
my $infile = shift // "color.txt";

open my $fin, '<', $infile or die "ファイル '$infile' が見つかりません.";

my @classBuf;
while ( <$fin> ) {
	chomp(my $line = $_);
	$line =~ /^(?<class>.*?) *= *(?<days>[\d ]*)$/;

	for my $day ( split / /, $+{days} ) {
		push @{ $classBuf[$day] }, $+{class};
	}
}

my $cal = calendar($month, $year);
my @caps = qw( 日 月 火 水 木 金 土 );
my @wdays = qw( sunday monday tuesday wednesday thursday friday saturday );
say qq(<table class="calendar">);
say qq(  <tr><td colspan="7">$year年$month月</td></tr>);

say qq(  <tr>);
for (my $i = 0; $i < 7; $i++) {
    my $cap = $caps[$i];
    my $wday = $wdays[$i];
	my $style =
	      $cap eq "日" ? qq( style="background-color: #FF3300; color: #FFFFFF;")
		: $cap eq "土" ? qq( style="background-color: #4169e1; color: #FFFFFF;")
		: "";
    say qq(    <th${style}>$cap</th>);
}
say qq(  </tr>);

foreach my $week ( @$cal ) {
    say qq(  <tr>);
    for (my $i = 0; $i < 7; $i++) {
        my $mday = $week->[$i] // "";
		my $rClasses = $mday eq "" ? [] : $classBuf[$mday];
        my $classes =
			( !defined $rClasses || scalar( @$rClasses ) == 0 )
			? ""
			: qq( class=") . (join ' ', @$rClasses) . qq(");
        say qq(    <td${classes}>$mday</td>);
    }
    say qq(  </tr>);
}

say qq(</table>);
